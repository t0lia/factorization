package com.apozdniakov.factorization;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class FactorizationController {

    private FactorizationService factorizationService;
    private InputParser inputParser;

    public FactorizationController(FactorizationService factorizationService, InputParser inputParser) {
        this.factorizationService = factorizationService;
        this.inputParser = inputParser;
    }

    // http -fb POST localhost:8080/factorize number=30
    @RequestMapping(value = "/factorize",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity createRole(@RequestBody MultiValueMap<String, String> formData) {
        List<Integer> result = factorizationService.factorize(inputParser.parse(formData));
        return ResponseEntity.ok(result);
    }

}
