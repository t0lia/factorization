package com.apozdniakov.factorization;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class FactorizationService {
    public List<Integer> factorize(int number) {
        int ceiling = number / 2 + 1;
        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 2; i < ceiling; ) {
            if (number % i == 0) {
                result.add(i);
                number /= i;
            } else {
                i++;
            }
        }
        return result.isEmpty() ? Collections.singletonList(number) : result;
    }
}
