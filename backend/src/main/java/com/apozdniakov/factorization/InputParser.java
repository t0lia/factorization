package com.apozdniakov.factorization;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class InputParser {

    public int parse(Map<String, List<String>> formData) {
        List<String> input = formData.get("number");
        if (input.isEmpty()) {
            throw new RuntimeException("empty input");
        }
        int number;
        try {
            number = Integer.parseInt(input.get(0));
        } catch (NumberFormatException nfe) {
            throw new RuntimeException(nfe);
        }
        return number;
    }
}
